package taskA;

import java.util.*;
import java.util.concurrent.*;

import simulation.*;

import java.nio.file.*;
import java.io.*;

public class TaskA {
	public static void main(String args[]) {
		//Simulation simulation = new Simulation();
		Parallelopolis parallelopolis = new Parallelopolis();

		int cyclesLeft = 60;
		long lastCycleTime = 0;
		while(cyclesLeft > 0) {
			if(System.currentTimeMillis() - lastCycleTime >= 1000) {
				cyclesLeft--;
				System.out.printf("Cycles Left: %d\n", cyclesLeft);
				parallelopolis.tick();
				lastCycleTime = System.currentTimeMillis();
			}
		}
		parallelopolis.endAndWriteOutputs();
	}
}