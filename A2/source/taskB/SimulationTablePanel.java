package taskB;

import java.awt.*;
import javax.swing.*;

public class SimulationTablePanel extends JPanel {
	public SimulationTablePanel(SimulationTableModel tableModel) {
		final JTable table = new JTable();
		
		table.setPreferredScrollableViewportSize(new Dimension(600, 200));
		table.setFillsViewportHeight(true);
		table.setShowHorizontalLines(false);
		table.setShowVerticalLines(false);;
		table.setModel(tableModel);
		
		JScrollPane scrollPane = new JScrollPane(table);
		
		add(scrollPane);
	}
}
