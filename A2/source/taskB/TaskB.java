package taskB;

import java.util.List;
import javax.swing.*;
import simulation.Parallelopolis;

public class TaskB {
	public static void main(String[] args) {
		Parallelopolis parallelopolis = new Parallelopolis();
		final SimulationTableModel tableModel = new SimulationTableModel(parallelopolis.getPolice());
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				SimulationFrame frame = new SimulationFrame("Parallelopolis");
				SimulationTablePanel tablePanel = new SimulationTablePanel(tableModel);
				frame.getContentPane().add(tablePanel);
				frame.setVisible(true);
			}
		});
		
		SwingWorker worker = new SwingWorker<Void, SimulationTableModel>() {
			public Void doInBackground() {
				int cyclesLeft = 60;
				long lastCycleTime = 0;
				while(cyclesLeft > 0) {
					if(System.currentTimeMillis() - lastCycleTime >= 1000) {
						cyclesLeft--;
						parallelopolis.tick();
						System.out.printf("Cycles Left: %d\n", cyclesLeft);
						lastCycleTime = System.currentTimeMillis();
						this.publish(tableModel);
					}
				}
				parallelopolis.end();
				return null;
			}
			
			protected void process(List<SimulationTableModel> tableModels) {
				for(SimulationTableModel model : tableModels) {
					model.fireTableDataChanged();
				}
			}
		};
		
		worker.execute();
		while(!worker.isDone()) {};
	}
}
