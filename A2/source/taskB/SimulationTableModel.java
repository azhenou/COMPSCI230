package taskB;

import simulation.Police;
import simulation.Suspect;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class SimulationTableModel extends AbstractTableModel {
	private String[] columnNames = {"ID", "Location", "Status", "Police Dog", "Suspect"};
	private ArrayList<Police> alPolice;
	
	public SimulationTableModel(ArrayList<Police> police) {
		alPolice = police;
	}
	
	public int getRowCount() {
		return alPolice.size();
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getValueAt(int row, int col) {
		Police police = alPolice.get(row);
		switch(col) {
		case 0:
			return police.getID();
		case 1:
			return String.format("(%d,%d)", police.getXLocation(), police.getYLocation());
		case 2:
			return police.getStatus().toString();
		case 3:
			return (police.getDog() ? "Yes" : "No");
		case 4:
			Suspect suspect = police.getSuspect();
			if(suspect != null) return suspect.getID();
			else return "";
		}
		return null;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
	}
}
