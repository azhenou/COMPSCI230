package taskB;

import java.awt.FlowLayout;
import javax.swing.JFrame;

public class SimulationFrame extends JFrame {
	public SimulationFrame(String name) {
		super(name);
		setLayout(new FlowLayout());
		setSize(800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
