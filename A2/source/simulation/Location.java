package simulation;

public class Location {
	public int xLocation;
	public int yLocation;

	public Location() {
		this(0, 0);
	}

	public Location(int x, int y) {
		xLocation = x;
		yLocation = y;
	}

	public static double distanceBetween(Location a, Location b) {
		return Math.sqrt(Math.pow(a.xLocation - b.xLocation, 2) + Math.pow(a.yLocation - b.yLocation, 2));
	}

	public static boolean equals(Location a, Location b) {
		return ((a.xLocation == b.xLocation) && (a.yLocation == b.yLocation));
	}

	public void setLocation(int x, int y) {
		xLocation = x;
		yLocation = y;
	}

	public void setLocation(Location a) {
		xLocation = a.xLocation;
		yLocation = a.yLocation; 
	}
	
	public int getXLocation() {
		return xLocation;
	}
	
	public int getYLocation() {
		return yLocation;
	}
}