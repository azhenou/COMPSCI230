package simulation;

public class AtKennelStatus extends PoliceStatus {
	public AtKennelStatus(Police police, Parallelopolis p) {
		super(police, p);
	}
	
	public String toString() {
		return "At Kennel";
	}
	
	public void simulate() {
		if(police.getDog()) {
			if(p.getKennel().returnDog()) {
				police.setDog(false);
				police.setStatus(new ReturningStatus(police, p));
			}
		} else {
			if(p.getKennel().loanDog()) {
				police.setDog(true);
				police.setStatus(new ApproachingSuspectStatus(police, p));
			}
		}
	}
}
