package simulation;

import java.util.*;
import java.util.concurrent.*;
import java.nio.file.*;
import java.io.*;

public class Parallelopolis {
	private ExecutorService pool;
	private ArrayList<Police> alPolice;
	private ArrayList<Suspect> alSuspect;
	private ArrayList<PoliceStation> alStation;
	private Kennel kennel;

	public Parallelopolis() {
		Police.setParallelopolis(this);
		alPolice = readPoliceCSV();
		alSuspect = readSuspectCSV(); 

		alStation = new ArrayList<PoliceStation>();
		alStation.add(new PoliceStation(25, 5));
		alStation.add(new PoliceStation(80, 30));
		alStation.add(new PoliceStation(10, 90));
		alStation.add(new PoliceStation(70, 80));

		pool = Executors.newCachedThreadPool();
		PoliceStation.setMaxCapacity((int) Math.ceil(alPolice.size() / 4.f));
		
		Kennel.setDogsAvailable((int) Math.ceil(alSuspect.size() / 2.f));
		kennel = new Kennel(50, 50);
	}
	
	//simulates one second in parallelopolis
	public void tick() {
		for(int i=0; i<alPolice.size(); i++) {
			pool.execute(alPolice.get(i));
		}
	}
	
	//shuts down parallelopolis
	public void end() {
		pool.shutdown();
	}

	public void endAndWriteOutputs() {
		pool.shutdown();
		try {
			if(pool.awaitTermination(60, TimeUnit.SECONDS)) {
				writePoliceCSV();
				writeSuspectCSV();
			}
		} catch (InterruptedException e) {
			System.out.println("Interrupted");
		}
	}

	//getters
	public ArrayList<Police> getPolice() {
		return alPolice;
	}
	
	public ArrayList<Suspect> getSuspect() {
		return alSuspect;
	}

	public ArrayList<PoliceStation> getStation() {
		return alStation;
	}

	public Kennel getKennel() {
		return kennel;
	}
	
	//Reading and Writing the CSV files
	private static ArrayList<Police> readPoliceCSV() {
		ArrayList<Police> alPolice = new ArrayList<Police>();

		Scanner input = null;
		Path path = Paths.get("police.csv");
		try {
			input = new Scanner(path);

		} catch (IOException ioExc) {
			System.out.println("File does not exist or is not readable");
		}

		if(input != null) {
			input.nextLine(); //get rid of headers in csv
			while(input.hasNext()) {
				String[] curLine = input.nextLine().split(",");
				String id = curLine[0];
				int x = Integer.parseInt(curLine[1]);
				int y = Integer.parseInt(curLine[2]);
				//assuming all status is Standby
				boolean dog = false; //assuming no police has dogs in the beginning since they are all on standby

				alPolice.add(new Police(id, x, y, dog));
			}
		}	

		return alPolice;
	}

	private static ArrayList<Suspect> readSuspectCSV() {
		ArrayList<Suspect> alSuspect = new ArrayList<Suspect>();

		Scanner input = null;
		Path path = Paths.get("suspects.csv");
		try {
			input = new Scanner(path);
		} catch (IOException ioexc) {
			System.out.println("file does not exists or is not readable");
			return alSuspect;
		}

		if(input != null) {
			input.nextLine(); //get rid of headers in csv
			while(input.hasNext()) {
				String[] curLine = input.nextLine().split(",");
				String id = curLine[0];
				int x = Integer.parseInt(curLine[1]);
				int y = Integer.parseInt(curLine[2]);
				Suspect.Status status = Suspect.Status.stringToStatus(curLine[3]);

				alSuspect.add(new Suspect(id, x, y, status));
			}
		}	

		return alSuspect;
	}

	private void writePoliceCSV() {
		System.out.println("Writing police-output.csv");
		Formatter output = null;
		try {
			output = new Formatter("police-output.csv");
		} catch (SecurityException e) {
			System.out.println("Cannot write to file");
		} catch (FileNotFoundException e) {
			System.out.println("Cannot find file");
		}

		if(output != null) {
			output.format("id,x.location,y.location,status,dog,suspect\r\n");
			for(int i=0; i<alPolice.size(); i++) {
				output.format("%s\r\n", alPolice.get(i).toCSVFormat());
			}
			output.close();
		}
		System.out.println("Finished writing police-output.csv");
	}

	private void writeSuspectCSV() {
		System.out.println("Writing suspect-output.csv");
		Formatter output = null;
		try {
			output = new Formatter("suspect-output.csv");
		} catch (SecurityException e) {
			System.out.println("Cannot write to file");
		} catch (FileNotFoundException e) {
			System.out.println("Cannot find file");
		}

		if(output != null) {
			output.format("id,x.location,y.location,status,police unit\r\n");
			for(int i=0; i<alSuspect.size(); i++) {
				Suspect curSuspect = (Suspect) alSuspect.get(i);
				output.format("%s\r\n", curSuspect.toCSVFormat());
			}
			output.close();
		}
		System.out.println("Finished writing suspect-output.csv");
	}
}