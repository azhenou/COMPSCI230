package simulation;

import java.util.*;
import java.nio.file.*;
import java.io.*;
import java.io.FileNotFoundException;
import java.lang.SecurityException;

public class Police extends Location implements Runnable {
	private static Parallelopolis parallelopolis;
	
	public static void setParallelopolis(Parallelopolis p) {
		parallelopolis = p;
	}

	private String id;
	private PoliceStatus status;
	private boolean dog;
	private Suspect suspect;
	
	private PoliceStation station;
	private Location destination;
	
	//constructors
	public Police() {
		super();
		id = "";
		status = new StandbyStatus(this, parallelopolis);
		dog = false;
		suspect = null;
		this.station = null;
	}

	public Police(String id, int x, int y, boolean dog) {
		super(x, y);
		this.id = id;
		this.status = new StandbyStatus(this, parallelopolis);
		this.dog = dog;
		suspect = null;
		this.station = null;
	}
	
	//Overriding run method for runnable interface
	public void run() {
		status.simulate();
	}

	public String toString() {
		if(suspect == null) {
			return String.format("%s %d %d %s %b %s", id, xLocation, yLocation, status, dog, null);
		} else {
			return String.format("%s %d %d %s %b %s", id, xLocation, yLocation, status, dog, suspect.getID());
		}
	}

	public String toCSVFormat() {
		if(suspect == null) {
			return String.format("%s,%d,%d,%s,%s,", id, xLocation, yLocation, status, (dog ? "Yes" : "No"));
		} else {
			return String.format("%s,%d,%d,%s,%s,%s", id, xLocation, yLocation, status, (dog ? "Yes" : "No"), suspect.getID());
		}
	}

	//setters and getters
	public String getID() {
		return id;
	}
	
	public PoliceStatus getStatus() {
		return status;
	}
	
	public void setStatus(PoliceStatus s) {
		status = s;
	}

	public boolean getDog() {
		return dog;
	}
	
	public void setDog(boolean d) {
		dog = d;
	}
	
	public Suspect getSuspect() {
		return suspect;
	}
	
	public void setSuspect(Suspect s) {
		suspect = s;
	}
	
	public Location getDestination() {
		return destination;
	}
	
	public void setDestination(Location newDestination) {
		if(destination != newDestination) 
			destination = newDestination;
	}
	
	public PoliceStation getStation() {
		return station;
	}
	
	public void setStation(PoliceStation s) {
		station = s;
	}

	//helper functions
	public boolean approachDestination(int moves) {
		for(int i=0; i<moves; i++) {
			if(xLocation < destination.xLocation) xLocation++;
			else if(xLocation > destination.xLocation) xLocation--;
			else if(yLocation < destination.yLocation) yLocation++;
			else if(yLocation > destination.yLocation) yLocation--;
		}
		if(hasCaughtSuspect()) {
			suspect.setLocation(this);
		}
		return Location.equals(this, destination);
	}

	private boolean hasCaughtSuspect() {
		if (suspect == null) return false;
		return suspect.getStatus() == Suspect.Status.CAUGHT;
	}
}