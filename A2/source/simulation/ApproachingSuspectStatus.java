package simulation;

public class ApproachingSuspectStatus extends PoliceStatus {
	public ApproachingSuspectStatus(Police police, Parallelopolis p) {
		super(police, p);
	}
	
	public String toString() {
		return "Approaching Suspect";
	}
	
	public void simulate() {
		police.setDestination(police.getSuspect());
		if(police.approachDestination(4)) {
			police.getSuspect().caught();
			police.setStatus(new AtSceneStatus(police, p));
		}
	}
}
