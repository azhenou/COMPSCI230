package simulation;

import java.util.ArrayList;

public class StandbyStatus extends PoliceStatus {
	public StandbyStatus(Police police, Parallelopolis p) {
		super(police, p);
	}
	
	public String toString() {
		return "Standby";
	}
	
	public void simulate() {
		if(setClosestSuspect(p.getSuspect(), police)){
			if(police.getStation() != null) {
				police.getStation().unassignStation();
				police.setStation(null);
			}
			police.setStatus(new ApproachingKennelStatus(police, p));
		}
	}
	
	//finds and returns the closest destination to police
	private static boolean setClosestSuspect(ArrayList<Suspect> suspects, Police police) {
		synchronized(suspects) {
			double minDistance = Double.MAX_VALUE;
			Suspect minSuspect = null;
			Suspect curSuspect;
			double distance;
	
			for(int i=0; i<suspects.size(); i++) {
				curSuspect = suspects.get(i);
				if(curSuspect.isUnassigned()) {
					distance = Location.distanceBetween(police, curSuspect);
					if(distance < minDistance) {
						minDistance = distance;
						minSuspect = curSuspect;
					}
				}
			}	
	
			if(minSuspect != null) {
				minSuspect.assigned(police);
				police.setSuspect(minSuspect);
				return true;
			}
			
			return false;
		}
	}
}
