package simulation;

public class Kennel extends Location {
	//instance variables
	private static int dogsAvailable;
	
	public static void setDogsAvailable(int i) {
		dogsAvailable = i;
	}
	
	int dogsLeft;

	//constructors
	public Kennel() {
		this(0, 0);
		dogsLeft = dogsAvailable;
	}

	public Kennel(int x, int y) {
		super(x, y);
		dogsLeft = dogsAvailable;
	}
	
	public String toString() {
		return String.format("Kennel: %d", dogsAvailable);
	}
	
	//Methods for police to get and return dogs to the kennel
	//returns true or false depending on whether the operation was successful or not
	public boolean returnDog() {
		synchronized(this) {
			dogsLeft++;
			return true;
		}
	}

	public boolean loanDog() {
		synchronized(this) {
			if(dogsLeft > 0) {
				dogsLeft--;
				return true;
			}
			return false;
		}
	}
}