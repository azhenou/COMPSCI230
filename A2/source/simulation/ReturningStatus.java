package simulation;

import java.util.ArrayList;

public class ReturningStatus extends PoliceStatus {
	public ReturningStatus(Police police, Parallelopolis p) {
		super(police, p);
	}
	
	public String toString() {
		return "Returning";
	}
	
	public void simulate() {
		if(police.getStation() == null) {
			setClosestStation(p.getStation(), police);
		}
		police.setDestination(police.getStation());
		if(police.approachDestination(3)) {
			police.setStatus(new StandbyStatus(police, p));
			police.getSuspect().jailed();
			police.setSuspect(null);
		}
	}
	
	private static void setClosestStation(ArrayList<PoliceStation> stations, Police police) {
		synchronized(stations) {
			double minDistance = Double.MAX_VALUE;
			PoliceStation minStation = null;
			PoliceStation curStation;
			double distance;

			for(int i=0; i<stations.size(); i++) {
				curStation = stations.get(i);
				if(curStation.isAvailable()) {
					distance = Location.distanceBetween(police, curStation);
					if(distance < minDistance) {
						minDistance = distance;
						minStation = curStation;
					}
				}
			}	

			if(minStation != null) {
				minStation.assignStation();
				police.setStation(minStation);
			}
		}
	}
}
