package simulation;

import java.util.ArrayList;

//An abstract class that is parent for all possible police statuses
//contains abstract method which will change the state of police depending on what status it is in
public abstract class PoliceStatus {
	protected Police police;
	protected Parallelopolis p;
	
	abstract void simulate();
	
	public PoliceStatus(Police police, Parallelopolis p) {
		this.police = police;
		this.p = p;
	}
}
