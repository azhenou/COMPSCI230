package simulation;

public class PoliceStation extends Location{
	private static int maxCapacity;
	
	public static void setMaxCapacity(int i) {
		maxCapacity = i;
	}
	
	private int n;

	public PoliceStation() {
		this(0, 0);
	}

	public PoliceStation(int x, int y) {
		super(x, y);
		n = 0;
	}
	
	//synchronized methods to assign a policeStation to police
	//needs to be synchronized so that policeStation don't get 'over assigned';
	public synchronized boolean isAvailable() {
		return n != maxCapacity;
	}

	public synchronized void assignStation() {
		n++;
	}

	public synchronized void unassignStation() {
		n--;
	}

	public String toString() {
		return String.format("Police Station: %d %d", xLocation, yLocation);
	}
}