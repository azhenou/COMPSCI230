package simulation;

import java.util.*;

public class Suspect extends Location{
	//enum class for the status of suspect
	public enum Status {
		UNASSIGNED ("Unassigned"),
		ASSIGNED ("Assigned"),
		CAUGHT ("Caught"),
		JAILED ("Jailed");

		private String text;
		
		Status(String t) {
			text = t;
		}	

		public String toString() {
			return text;
		}

		public static Status stringToStatus(String input) {
			for (Status s : Status.values()) {
				if(s.toString().equals(input)) {
					return s;
				}
			}
			return UNASSIGNED;
		}
	}

	private String id;
	private Status status;
	private Police police;

	//Constructors
	public Suspect() {
		super();
		id = "";
		status = Status.UNASSIGNED;
		police = null;
	}

	public Suspect(String id, int x, int y, Status status) {
		super(x, y);
		this.id = id;
		this.status = status;
		this.police = null;
	}

	//toString, getters, setters
	public String toString() {
		if(police == null) {
			return String.format("%s %d %d %s %s", id, xLocation, yLocation, status, null);
		} else {
			return String.format("%s %d %d %s %s", id, xLocation, yLocation, status, police.getID());
		}
	}

	public String toCSVFormat() {
		if(police == null) {
			return String.format("%s,%d,%d,%s", id, xLocation, yLocation, status);
		} else {
			return String.format("%s,%d,%d,%s,%s", id, xLocation, yLocation, status, police.getID());
		}
	}

	public String getID() {
		return id;
	}

	public Status getStatus() {
		return status;
	}

	//methods to change the suspects status
	//synchronized so that two police cannot be assigned the same police
	public synchronized boolean isUnassigned() {
		return status == Suspect.Status.UNASSIGNED;
	}
	
	public synchronized void assigned(Police p) {
		status = Suspect.Status.ASSIGNED;
		police = p;
	}

	public synchronized void caught() {
		status = Suspect.Status.CAUGHT;
	}

	public synchronized void jailed() {
		status = Suspect.Status.JAILED;
		police = null;
	}
}