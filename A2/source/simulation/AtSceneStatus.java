package simulation;

public class AtSceneStatus extends PoliceStatus {
	private static int waitTime = 4;
	int timeLeft;
	
	public AtSceneStatus(Police police, Parallelopolis p) {
		super(police, p);
		timeLeft = waitTime;
	}
	
	public String toString() {
		return "At Scene";
	}
	
	public void simulate() {
		if(timeLeft == 0) {
			police.setStatus(new ApproachingKennelStatus(police, p));
		} else {
			timeLeft--;
		}
	}

}
