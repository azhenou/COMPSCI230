package simulation;

public class ApproachingKennelStatus extends PoliceStatus {
	public ApproachingKennelStatus(Police police, Parallelopolis p) {
		super(police, p);
	}
	
	public String toString() {
		return "Approaching Kennel";
	}
	
	public void simulate() {
		police.setDestination(p.getKennel());
		if(police.approachDestination(3)) {
			police.setStatus(new AtKennelStatus(police, p));
		}
	}
}
