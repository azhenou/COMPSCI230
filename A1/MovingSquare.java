//Albert Zhen Hui Ou
//737996211

/*
 *	===============================================================================
 *	MovingSquare.java : A shape that is a square.
 *	A rectangle has 4 handles shown when it is selected (by clicking on it).
 *	===============================================================================
 */

import java.awt.*;
public class MovingSquare extends MovingRectangle {
	public MovingSquare() {
		this(10, 20, 50, 50, 500, 500, Color.orange, Color.yellow, Path.FALLING); //default
	}
	
	public MovingSquare(int x, int y, int w, int h, int mw, int mh, Color bc, Color fc, Path pathType) {
		super(x, y, w, h, mw, mh, bc, fc, pathType);
		setLength(Math.min(w,h));
	}
	
	//set the width and height of the square
	private void setLength(int x) {
		width = x;
		height = x;
	}
	
	//Overriding setWidth method from the MovingShape abstract superclass
	@Override
	public void setWidth(int w) {
		setLength(w);
	}
	
	//Overiding setHeight method from the MovingShape abstract superclass
	@Override
	public void setHeight(int h) {
		setLength(h);
	}
}
