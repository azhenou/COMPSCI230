//Albert Zhen Hui Ou
//737996211

/*
 *	===============================================================================
 *	MovingFace.java : A shape that is a face.
 *  The border color is only used for the outline
 *  the fill color is used for the features (eyes and mouth)
 *	A Face has 4 handles shown when it is selected (by clicking on it).
 *	Made up of 4 features (face, left eye, right eye, mouth)
 *	===============================================================================
 */

import java.awt.*;
public class MovingFace extends MovingOval {
	//relative coordinates of features in a 100x100 MovingFace
	//contains the relative coordinates of face, left eye, right eye, and mouth
	private static Point[] relativeCoordinates = new Point[] {new Point(0, 0), new Point(20,25), new Point(60,25), new Point(20, 45)};
	//relative size of features in a 100x100 MovingFace
	//contains the relative size of face, left eye, right eye, and mouth
	private static Point[] relativeSize = new Point[] {new Point(100, 100), new Point(20,20), new Point(20,20), new Point(60,30)};
	private double xScale, yScale; //The xScaling and yScaling of the Face
	private Feature[] features = new Feature[] {new Face(), new Eye(), new Eye(), new Mouth()};
	
	public MovingFace() {
		this(10, 20, 50, 50, 500, 500, Color.orange, Color.yellow, Path.FALLING); //default
	}
	
	public MovingFace(int x, int y, int w, int h, int mw, int mh, Color bc, Color fc, Path pathType) {
		super(x, y, w, h, mw, mh, bc, fc, pathType);
		xScale = width / 100.0;
		yScale = height / 100.0;
	}
	
	//Overiding the setWidth method
	//xScale also needs to be updated when the width is changed
	@Override
	public void setWidth(int w) {
		width = w;
		xScale = width / 100.0;
	}
	
	//Overiding the setHeight method
	//yScale also needs to be updated when the height is changed
	@Override
	public void setHeight(int h) {
		height = h;
		yScale = height / 100.0;
	}
	
	/** draw the face
	 *  the border color is only used for the border
	 *  the fill color is used for the features (eyes and mouth)
	 *	If it is selected, draw the handles
	 *	@param g	the Graphics control
	 */
	void draw(Graphics g) {
		for(int i=0; i<4; i++) {
			int absoluteX = x + (int)(relativeCoordinates[i].getX() * xScale);
			int absoluteY = y + (int)(relativeCoordinates[i].getY() * yScale);
			int absoluteW = (int)(relativeSize[i].getX() * xScale);
			int absoluteH = (int)(relativeSize[i].getY() * yScale);
			features[i].draw(g, absoluteX, absoluteY, absoluteW, absoluteH);
		}
		drawHandles(g);
	}
	
	//A nested abstract superclass that is only used for the MovingFace class
	//Each subclass of feature has their own draw function
	public abstract class Feature {
		abstract void draw(Graphics g, int x, int y, int w, int h);
	}
	
	public class Face extends Feature {
		public void draw(Graphics g, int x, int y, int w, int h) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setPaint(borderColor);
			g2d.setStroke(new BasicStroke((float)(5.0 * Math.min(xScale, yScale))));
			g2d.drawOval(x, y, w, h);
			g2d.setStroke(new BasicStroke(1.0f)); //reset the stroke
		}
	}
	
	public class Eye extends Feature {
		public void draw(Graphics g, int x, int y, int w, int h) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setPaint(fillColor);
			g2d.fillOval(x, y, w, h);
		}
	}
	
	public class Mouth extends Feature {
		public void draw(Graphics g, int x, int y, int w, int h) {
			Graphics2D g2d = (Graphics2D) g;
			g2d.setPaint(fillColor);
			g2d.setStroke(new BasicStroke((float)(5.0 * Math.min(xScale, yScale))));
			g2d.drawArc(x, y, w, h, 210, 120);
			g2d.setStroke(new BasicStroke(1.0f)); //reset the stroke
		}
	}
}
