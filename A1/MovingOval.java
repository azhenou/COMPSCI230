//Albert Zhen Hui Ou
//737996211

/*
 *	===============================================================================
 *	MovingOval.java : A shape that is a oval.
 *	A oval has 4 handles shown when it is selected (by clicking on it).
 *	===============================================================================
 */

import java.awt.*;
public class MovingOval extends MovingShape {
	public MovingOval() {
		this(10, 20, 50, 20, 500, 500, Color.orange, Color.yellow, Path.FALLING); //default
	}
	
	public MovingOval(int x, int y, int w, int h, int mw, int mh, Color bc, Color fc, Path pathType) {
		super(x, y, w, h, mw, mh, bc, fc, pathType);
	}
	
	/** Returns whether the point is in the oval or not
	 * @return true if and only if the point is in the oval, false otherwise.
	 */
	public boolean contains(Point mousePt) {
		Point EndPt = new Point(x + width, y+height);
		double dx = (2 * mousePt.x - x - EndPt.x) / (double) width;
		double dy = (2 * mousePt.y - y - EndPt.y) / (double) height;
		return dx * dx + dy * dy < 1.0;
	}
	
	/** draw the oval with the fill colour
	 *	If it is selected, draw the handles
	 *	@param g	the Graphics control
	 */
	void draw(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setPaint(fillColor);
		g2d.fillOval(x, y, width, height);
		g2d.setPaint(borderColor);
		g2d.drawOval(x, y, width, height);
		drawHandles(g);
	}
}
